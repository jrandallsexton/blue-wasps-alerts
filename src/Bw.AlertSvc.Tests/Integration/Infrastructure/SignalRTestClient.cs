﻿
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Azure.SignalR.Management;

using System;
using System.Threading.Tasks;

namespace Bw.AlertSvc.Tests.Integration.Infrastructure
{
    public class SignalRTestClient
    {
        private readonly string _url;
        private readonly string _accessToken;

        private HubConnection _connection;

        public SignalRTestClient(string hubName, string connectionString, string userName)
        {
            var svcMgr = new ServiceManagerBuilder()
                .WithOptions(o => o.ConnectionString = connectionString)
                .Build();

            _url = svcMgr.GetClientEndpoint(hubName);
            _accessToken = svcMgr.GenerateClientAccessToken(hubName, userName);
        }

        public async Task CreateHubConnectionAsync(string methodName, Action<string> onConnected)
        {
            _connection = new HubConnectionBuilder()
                .WithUrl(_url, option =>
                {
                    option.AccessTokenProvider = () => Task.FromResult(_accessToken);
                }).Build();
            _connection.On(methodName, onConnected);
            await _connection.StartAsync();
        }

        public async Task StopHubConnectionAsync()
        {
            await _connection.StopAsync();
        }
    }
}