﻿using Bw.AlertSvc.Dtos;

using System.Threading.Tasks;

namespace Bw.AlertSvc.Infrastructure
{
    public interface IProvideAlerts
    {
        Task BroadcastAlertAsync(RealTimeAlert alert);
    }
}