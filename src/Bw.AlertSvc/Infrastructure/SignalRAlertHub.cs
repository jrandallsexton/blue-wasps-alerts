﻿using Bw.AlertSvc.Dtos;
using Bw.Eventing.Framework.Common;

using Microsoft.AspNetCore.SignalR;
using Microsoft.Azure.SignalR.Management;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using System.Threading.Tasks;

namespace Bw.AlertSvc.Infrastructure
{
    public class SignalRAlertHub : Hub, IProvideAlerts
    {
        private readonly string _connectionString;
        private IServiceHubContext _hubContext;
        private const string _hubName = nameof(SignalRAlertHub);

        public SignalRAlertHub(IConfiguration config)
        {
            _connectionString = config["Azure:SignalR:ConnectionString"];
        }

        private async Task InitAsync()
        {
            if (_hubContext == null)
                return;

            //var svcMgr = new ServiceManagerBuilder().WithOptions(options =>
            //{
            //    options.ConnectionString = _connectionString;
            //    options.ServiceTransportType = ServiceTransportType.Persistent;
            //}).Build();

            var svcMgr = new ServiceManagerBuilder().WithOptions(options =>
            {
                options.ConnectionString = _connectionString;
                options.ServiceTransportType = ServiceTransportType.Persistent;
            }).Build();

            _hubContext = await svcMgr.CreateHubContextAsync(_hubName, new LoggerFactory());
        }

        public async Task BroadcastAlertAsync(RealTimeAlert alert)
        {
            await InitAsync();
            await _hubContext.Clients.All.SendAsync(nameof(RealTimeAlert), alert.ToJson());
        }
    }
}