﻿using Bw.Eventing.Framework;

using Microsoft.EntityFrameworkCore;

namespace Bw.AlertSvc.Data
{
    public class AlertDataContext : DbContext, IEventingDbContext
    {
        public DbSet<OutgoingEvent> OutgoingEvents { get; set; }
        public DbSet<IncomingEvent> IncomingEvents { get; set; }
    }
}